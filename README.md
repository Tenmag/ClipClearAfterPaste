# ClipClearAfterPaste

### Clear the Windows Clipboard After Paste for your security

---

If you want to clear the Windows clipboard for security reasons, just run [ClipClearAfterPaste](https://gitlab.com/Tenmag/ClipClearAfterPaste/-/releases). Every time you make a Ctrl+V, after the paste it clears the clipboard automatically, so you don't forget to leave sensitive information on your clipboard for sneaky softwares and websites.

You can compile the source code with AutoHotkey.

---

[Donate/Támogatás](https://paypal.me/Tenmag)

